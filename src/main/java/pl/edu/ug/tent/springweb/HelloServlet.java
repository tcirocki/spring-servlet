package pl.edu.ug.tent.springweb;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloServlet extends HttpServlet {


  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      
    System.out.println("doGet method invoked");

    doAction(req, resp);
  }
  
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      
    System.out.println("doPost method invoked");
    
    doAction(req, resp);
  }
  
  protected void doAction(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      
    resp.setContentType("application/json");
    PrintWriter out  = resp.getWriter();

    String name = req.getParameter("name");

    out.print("Witaj " + name);
  }
  
  
}

